'use strict'
const list = document.querySelector('.tabs');
list.addEventListener('click', function (element){
    let target = element.target.closest('.tabs-title')
    if (target){
        document.querySelectorAll('.active').forEach(el =>{
            el.classList.remove('active')
        })
        target.classList.add('active');

        const id = target.getAttribute('data-id');
        document.querySelector(id).classList.add('active')
    }
})